# EMF for pure OSGi

EMF is one of the most powerful MDSD tools. It Unfortunatelly comes with strong ties to Eclipse and Equinox, because it uses Extension Points. It can be used in Java SE and other OSGi frameworks, but it usually requires a lot of manual, to register EPackages, ResoureFactories etc.

## GeckoEMF

GeckoEMF sets out to provide a way to use EMF in pure OSGi environments regardless of the framework you use. It is an extension on top of EMF, so EMF can be used as before, without any changes. The Project is based on the eModelling Project of Bryan Hunt (https://github.com/BryanHunt/eModeling).

### Features

- Registration of EPackages via Whiteboard Services
- Registration of ResourceFactorys via Whiteboard Services
- Individual ResourceSet Configurators via Whiteboard Service
- ResourceSetFactory to get a individually configured ResourceSet
- Support for Requirements and Capabilities
- Multiple model Version at Runtime

- Additionally there is tooling support, to generate the EMF-configurator whiteboards from an existing genmodel. This tooling comes from an separate project geckoEMF-Tooling

### Requirements

As moste things in life, geckoEMF comes with some strings attached. 

- Eclipse
- BND and BNDTools
- OSGi R7
- Component Annotations

### How does it work

#### ResourceSet Factory

GeckoEMF registers a ResourceSetFactory Service under the Interface org.gecko.emf.osgi.ResourceSetFactory. it must be used a acquire new ResourceSets.

```java
@Component
public class Component
{

  @Reference
  private ResourceSetFactory factory;

  @Activate
  public void activate()
  {
    ResourceSet resourceSet = factory.createResourceSet();
    ...
  }

}
```

The ResourceSet contains a special EPackage.Registry and ResourceFactoryRegistry that is configured at runtime by Configurators. To enable your existing model packages you need to supplement your model bundles witha service as follows:

```java
@Component(name="MyEPackageConfigurator", service= {EPackageConfigurator.class, ResourceFactoryConfigurator.class})
@EMFModel(emf_model_name=MyEPackagePackage.eNAME, emf_model_nsURI={MyEPackagePackage.eNS_URI}, emf_model_version="1.0")
@RequireEMF
@ProvideEMFModel(name = MyEPackagePackage.eNAME, nsURI = { MyEPackagePackage.eNS_URI }, version = "1.0" )
@ProvideEMFResourceConfigurator( name = MyEPackagePackage.eNAME,
	contentType = { "my-epackager#1.0.0" }, 
	fileExtension = {
	"myepackage"
 	},  
	version = "1.0"
)
public class MyEPackageConfigurationComponent implements EPackageConfigurator, ResourceFactoryConfigurator {

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.ResourceFactoryConfigurator#configureResourceFactory(org.eclipse.emf.ecore.resource.Resource.Factory.Registry)
	 * @generated
	 */
	@Override
	public void configureResourceFactory(Registry registry) {
		registry.getExtensionToFactoryMap().put("myepackage", new MyEPackageResourceFactoryImpl()); 
		registry.getContentTypeToFactoryMap().put("my-epackage#1.0.0", new MyEPackageResourceFactoryImpl()); 
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.ResourceFactoryConfigurator#unconfigureResourceFactory(org.eclipse.emf.ecore.resource.Resource.Factory.Registry)
	 * @generated
	 */
	@Override
	public void unconfigureResourceFactory(Registry registry) {
		registry.getExtensionToFactoryMap().remove("myepackage"); 
		registry.getContentTypeToFactoryMap().remove("my-epackage#1.0.0"); 
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.EPackageRegistryConfigurator#configureEPackage(org.eclipse.emf.ecore.EPackage.Registry)
	 * @generated
	 */
	@Override
	public void configureEPackage(org.eclipse.emf.ecore.EPackage.Registry registry) {
		registry.put(MyEPackagePackage.eNS_URI, MyEPackagePackage.eINSTANCE);
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.EPackageRegistryConfigurator#unconfigureEPackage(org.eclipse.emf.ecore.EPackage.Registry)
	 * @generated
	 */
	@Override
	public void unconfigureEPackage(org.eclipse.emf.ecore.EPackage.Registry registry) {
		registry.remove(MyEPackagePackage.eNS_URI);
	}
	
}
```

#### Tooling Support

GeckoEMF offers a Eclipse Plugin, that modifies the EMF Model Codegenerator. The  Plugin can be installed from our [P2 Updatesite](http://devel.data-in-motion.biz/public/repository/gecko/release/geckoEMF.eclipse.tooling/).

If the genmodel option "OSGi Compatible" is st to true, the generator will: 
- Create Configurator Services for your Package.
- a bnd file
- adds the bnd nature to your project and removes the BND related natures if present
- MANIFEST.MF and plugin.xml are still generated
- The Model Code will be generated as one knows it from EMF

The corresponding project that build the tools can be found here:

https://gitlab.com/gecko.io/geckoEMF-Tooling

#### Artifacts

To use GeckoEMF you can eather use the [OBR](http://devel.data-in-motion.biz/public/repository/gecko/release/geckoEMF/) or our [Nexus](http://devel.data-in-motion.biz/nexus/repository/maven-releases/).

The following Artifacts are required from the [Nexus](http://devel.data-in-motion.biz/nexus/repository/maven-releases/).

- org.gecko.emf:org.gecko.emf.osgi.api
- org.gecko.emf:org.gecko.emf.osgi.annotations
- org.gecko.emf:org.gecko.emf.osgi.component
- org.gecko.emf:org.gecko.emf.osgi.registry
- org.gecko.emf:org.gecko.emf.osgi.ecore

# Contact

Feel free to file Bugs and for support contact us directly via the mailing list at gecko-emf@googlegroups.com
