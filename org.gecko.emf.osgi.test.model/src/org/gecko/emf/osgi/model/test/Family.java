/**
 */
package org.gecko.emf.osgi.model.test;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Family</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.emf.osgi.model.test.Family#getFather <em>Father</em>}</li>
 *   <li>{@link org.gecko.emf.osgi.model.test.Family#getMother <em>Mother</em>}</li>
 *   <li>{@link org.gecko.emf.osgi.model.test.Family#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @see org.gecko.emf.osgi.model.test.TestPackage#getFamily()
 * @model
 * @generated
 */
public interface Family extends EObject {
	/**
	 * Returns the value of the '<em><b>Father</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Father</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Father</em>' reference.
	 * @see #setFather(Person)
	 * @see org.gecko.emf.osgi.model.test.TestPackage#getFamily_Father()
	 * @model
	 * @generated
	 */
	Person getFather();

	/**
	 * Sets the value of the '{@link org.gecko.emf.osgi.model.test.Family#getFather <em>Father</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Father</em>' reference.
	 * @see #getFather()
	 * @generated
	 */
	void setFather(Person value);

	/**
	 * Returns the value of the '<em><b>Mother</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mother</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mother</em>' reference.
	 * @see #setMother(Person)
	 * @see org.gecko.emf.osgi.model.test.TestPackage#getFamily_Mother()
	 * @model
	 * @generated
	 */
	Person getMother();

	/**
	 * Sets the value of the '{@link org.gecko.emf.osgi.model.test.Family#getMother <em>Mother</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mother</em>' reference.
	 * @see #getMother()
	 * @generated
	 */
	void setMother(Person value);

	/**
	 * Returns the value of the '<em><b>Children</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.emf.osgi.model.test.Person}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' reference list.
	 * @see org.gecko.emf.osgi.model.test.TestPackage#getFamily_Children()
	 * @model
	 * @generated
	 */
	EList<Person> getChildren();

} // Family
