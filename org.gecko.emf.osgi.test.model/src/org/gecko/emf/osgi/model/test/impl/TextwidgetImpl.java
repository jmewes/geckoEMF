/**
 */
package org.gecko.emf.osgi.model.test.impl;

import org.eclipse.emf.ecore.EClass;

import org.gecko.emf.osgi.model.test.TestPackage;
import org.gecko.emf.osgi.model.test.Textwidget;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Textwidget</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TextwidgetImpl extends ContentImpl implements Textwidget {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TextwidgetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestPackage.Literals.TEXTWIDGET;
	}

} //TextwidgetImpl
