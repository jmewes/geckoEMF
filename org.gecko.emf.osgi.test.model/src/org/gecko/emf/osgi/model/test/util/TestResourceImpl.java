/**
 */
package org.gecko.emf.osgi.model.test.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.gecko.emf.osgi.model.test.util.TestResourceFactoryImpl
 * @generated
 */
public class TestResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public TestResourceImpl(URI uri) {
		super(uri);
	}

} //TestResourceImpl
