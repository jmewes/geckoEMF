/**
 */
package org.gecko.emf.osgi.model.test;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Textwidget</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.gecko.emf.osgi.model.test.TestPackage#getTextwidget()
 * @model
 * @generated
 */
public interface Textwidget extends Content {
} // Textwidget
