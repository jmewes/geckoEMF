/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi;

import org.gecko.emf.osgi.annotation.EMFModel;
import org.gecko.emf.osgi.annotation.EMFResourceSetConfigurator;

/**
 * 
 * @author Juergen Albert
 * @since 9 Feb 2018
 */
public class EMFNamespaces {

	public static final String EMF_NAMESPACE = "emf.core";
	public static final String EMF_CONFIGURATOR_NAMESPACE = "emf.configurator";
	
	/**
	 * Attribute name constants for {@link EMFResourceFactoryConfigurator} annotation
	 */

	// Attribute name for the ResourceSetFactory configurator name
	public static final String EMF_RESOURCE_CONFIGURATOR_NAME = "emf.resource.configurator.name";
	// Attribute name for the ResourceSetFactory configurator content type
	public static final String EMF_RESOURCE_CONFIGURATOR_CONTENT_TYPE = "emf.resource.configurator.contentType";
	// Attribute name for the ResourceSetFactory configurator file extension
	public static final String EMF_RESOURCE_CONFIGURATOR_FILE_EXT = "emf.resource.configurator.fileExtension";
	// Attribute name for the ResourceSetFactory configurator feature
	public static final String EMF_RESOURCE_CONFIGURATOR_FEATURE = "emf.resource.configurator.feature";

	/**
	 * Attribute name constants for {@link EMFResourceSetConfigurator} annotation
	 */
	
	// Attribute name for the ResourceSetFactory configurator name
	public static final String EMF_CONFIGURATOR_NAME = "emf.configurator.name";
	// Attribute name for the ResourceSetFactory configurator content type
	public static final String EMF_CONFIGURATOR_CONTENT_TYPE = "emf.configurator.contentType";
	// Attribute name for the ResourceSetFactory configurator file extension
	public static final String EMF_CONFIGURATOR_FILE_EXT = "emf.configurator.fileExtension";
	// Attribute name for the ResourceSetFactory configurator feature
	public static final String EMF_CONFIGURATOR_FEATURE = "emf.configurator.feature";
	
	/**
	 * Attribute name constants for {@link EMFModel} annotation
	 */
	
	// Attribute name for the EMF model name
	public static final String EMF_MODEL_NAME = "emf.model.name";
	// Attribute name for the EMF model namespace
	public static final String EMF_MODEL_NSURI = "emf.model.nsURI";
	// Attribute name for the EMF model content type
	public static final String EMF_MODEL_CONTENT_TYPE = "emf.model.contentType";
	// Attribute name for the EMF model file extension
	public static final String EMF_MODEL_FILE_EXT = "emf.model.fileExtension";
	// Attribute name for the EMF model version
	public static final String EMF_MODEL_VERSION = "emf.model.veriosn";
	
}
