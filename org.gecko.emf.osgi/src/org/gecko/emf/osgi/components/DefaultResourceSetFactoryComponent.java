/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Factory;
import org.eclipse.emf.ecore.resource.Resource.Factory.Registry;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.gecko.emf.osgi.EMFNamespaces;
import org.gecko.emf.osgi.EPackageConfigurator;
import org.gecko.emf.osgi.ResourceFactoryConfigurator;
import org.gecko.emf.osgi.ResourceSetConfigurator;
import org.gecko.emf.osgi.ResourceSetFactory;
import org.osgi.annotation.bundle.Capability;
import org.osgi.annotation.bundle.Requirement;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentConstants;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

/**
 * Implementation of a {@link ResourceSetFactory}. It hold the {@link EPackage} registry as well as the {@link Factory} registry.
 * {@link EPackage} are dynamically injected as {@link EPackageProvider} instance. 
 * {@link Factory} instance are injected dynamically as {@link ServiceReference} instance. So they can be registered using
 * their properties for contentTyp or fileExtension.
 * Third additional {@link ResourceSetConfigurator} instance can be injected to customize the {@link ResourceSet} for
 * further extension like custom serialization. 
 * @author Mark Hoffmann
 * @since 28.06.2017
 */
@Component(name="DefaultResourcesetFactory", enabled=true, immediate=true, service= {})
@Capability(
		namespace = EMFNamespaces.EMF_NAMESPACE,
		name = ResourceSetFactory.EMF_CAPABILITY_NAME,
		version = ResourceSetFactory.GECKO_EMF_VERSION
		)
@Requirement(namespace = EMFNamespaces.EMF_CONFIGURATOR_NAMESPACE, //
	name = EPackageConfigurator.EMF_CONFIGURATOR_NAME, filter="(name=ecore)")
//@RequireModelByName(name = "ecore", version = "1.0")
public class DefaultResourceSetFactoryComponent implements ResourceSetFactory {

	private final Set<ResourceSetConfigurator> resourceSetConfigurators = new CopyOnWriteArraySet<>();
	private final Set<EPackageConfigurator> ePackageConfigurators = new CopyOnWriteArraySet<>();
	private final Set<ResourceFactoryConfigurator> resourceFactoryConfigurators = new CopyOnWriteArraySet<>();
	private final List<String> configuratorNameList = new LinkedList<String>();
	private final List<String> resourceFactoryNameList = new LinkedList<String>();
	private final List<String> modelNameList = new LinkedList<String>();
	private volatile EPackage.Registry packageRegistry;
	private volatile Resource.Factory.Registry resourceFactoryRegistry;
	private ServiceRegistration<ResourceSetFactory> rsfRegistration = null;
	
	/**
	 * Inject the {@link EPackage.Registry}
	 * @param registry the registry to inject
	 */
	@Reference(policy=ReferencePolicy.STATIC, cardinality=ReferenceCardinality.MANDATORY, unbind="unsetRegistry")
	public void setRegistry(EPackage.Registry registry) {
		this.packageRegistry = registry;
		updatePackageRegistry();
	}

	/**
	 * Remove the registry on shutdown
	 * @param registry the registry to be removed
	 */
	public void unsetRegistry(EPackage.Registry registry) {
		this.packageRegistry.clear();
		this.packageRegistry = null;
	}

	/**
	 * Inject a {@link Registry} for resource factories
	 * @param resourceFactoryRegistry the resource factory to be injected
	 */
	@Reference(policy=ReferencePolicy.STATIC, unbind="unsetResourceFactoryRegistry")
	public void setResourceFactoryRegistry(Resource.Factory.Registry resourceFactoryRegistry) {
		this.resourceFactoryRegistry = resourceFactoryRegistry;
		updateResourceFactoryRegistry();
	}

	/**
	 * Removed the registry on shutdown
	 * @param resourceFactoryRegistry the registry to be removed
	 */
	public void unsetResourceFactoryRegistry(Resource.Factory.Registry resourceFactoryRegistry) {
		this.resourceFactoryRegistry.getExtensionToFactoryMap().clear();
		this.resourceFactoryRegistry.getContentTypeToFactoryMap().clear();
		this.resourceFactoryRegistry.getProtocolToFactoryMap().clear();
		this.resourceFactoryRegistry = null;
	}

	/**
	 * Injects {@link EPackageConfigurator}, to register the Ecore Package
	 * @param configurator the {@link EPackageConfigurator} to be registered
	 * @param properties the service properties
	 */
	@Reference(unbind ="removeEPackageConfigurator", policy=ReferencePolicy.DYNAMIC, cardinality=ReferenceCardinality.AT_LEAST_ONE, target="(emf.model.name=ecore)")
	public void addEcoreConfigurator(EPackageConfigurator configurator, Map<String, Object> properties) {
		addEPackageConfigurator(configurator, properties);
	}

	/**
	 * Injects {@link EPackageConfigurator}, to register a new {@link EPackage}
	 * @param configurator the {@link EPackageConfigurator} to be registered
	 * @param properties the service properties
	 */
	@Reference(policy=ReferencePolicy.DYNAMIC, cardinality=ReferenceCardinality.MULTIPLE, target="(!(emf.model.name=ecore))")
	public void addEPackageConfigurator(EPackageConfigurator configurator, Map<String, Object> properties) {
		ePackageConfigurators.add(configurator);
		if (packageRegistry != null) {
			configurator.configureEPackage(packageRegistry);
		}
		updateProperties(EMFNamespaces.EMF_MODEL_NAME, properties, true);
	}

	/**
	 * Removes a {@link EPackageConfigurator} from the registry and unconfigures it
	 * @param configurator the configurator to be removed
	 * @param properties the service properties
	 */
	public void removeEPackageConfigurator(EPackageConfigurator configurator, Map<String, Object> properties) {
		ePackageConfigurators.remove(configurator);
		updateProperties(EMFNamespaces.EMF_MODEL_NAME, properties, false);
		if (packageRegistry != null) {
			configurator.unconfigureEPackage(packageRegistry);
		}
	}

	/**
	 * Adds a resource factory configurator for the basic Ecore Package
	 * @param configurator the resource factory configurator to be registered
	 * @param properties the service properties
	 */
	@Reference(unbind = "removeResourceFactoryConfigurator", policy=ReferencePolicy.DYNAMIC, cardinality=ReferenceCardinality.AT_LEAST_ONE, target="(emf.model.name=ecore)")
	public void addEcoreResourceFactoryConfigurator(ResourceFactoryConfigurator configurator, Map<String, Object> properties) {
		addResourceFactoryConfigurator(configurator, properties);
	}

	/**
	 * Adds a resource factory configurator to the registry 
	 * @param configurator the resource factory configurator to be registered
	 * @param properties the service properties
	 */
	@Reference(policy=ReferencePolicy.DYNAMIC, cardinality=ReferenceCardinality.MULTIPLE, target="(!(emf.model.name=ecore))")
	public void addResourceFactoryConfigurator(ResourceFactoryConfigurator configurator, Map<String, Object> properties) {
		resourceFactoryConfigurators.add(configurator);
		if (resourceFactoryRegistry != null) {
			configurator.configureResourceFactory(resourceFactoryRegistry);
		}
		updateProperties(EMFNamespaces.EMF_RESOURCE_CONFIGURATOR_NAME, properties, true);
	}

	/**
	 * Removes a resource factory configurator from the registry
	 * @param configurator the resource factory configurator to be removed
	 * @param properties the service properties
	 */
	public void removeResourceFactoryConfigurator(ResourceFactoryConfigurator configurator, Map<String, Object> properties) {
		resourceFactoryConfigurators.remove(configurator);
		updateProperties(EMFNamespaces.EMF_RESOURCE_CONFIGURATOR_NAME, properties, false);
		if (resourceFactoryRegistry != null) {
			configurator.unconfigureResourceFactory(resourceFactoryRegistry);
		}
	}

	/**
	 * Adds new {@link ResourceSetConfigurator} to this factory
	 * @param resourceSetConfigurator the new configurator to be added
	 * @param properties the service properties
	 */
	@Reference(policy=ReferencePolicy.DYNAMIC, cardinality=ReferenceCardinality.MULTIPLE)
	public void addResourceSetConfigurator(ResourceSetConfigurator resourceSetConfigurator, Map<String, Object> properties) {
		resourceSetConfigurators.add(resourceSetConfigurator);
		updateProperties(EMFNamespaces.EMF_CONFIGURATOR_NAME, properties, true);
	}

	/**
	 * Removes a {@link ResourceSetConfigurator} from the list for this factory
	 * @param resourceSetConfigurator
	 * @param properties the service properties
	 */
	public void removeResourceSetConfigurator(ResourceSetConfigurator resourceSetConfigurator, Map<String, Object> properties) {
		resourceSetConfigurators.remove(resourceSetConfigurator);
		updateProperties(EMFNamespaces.EMF_CONFIGURATOR_NAME, properties, false);
	}

	/**
	 * Called on component activation
	 * @param ctx the component context
	 */
	@Activate
	public void activate(ComponentContext ctx) {
		packageRegistry.putAll(EPackage.Registry.INSTANCE);
		resourceFactoryRegistry.getExtensionToFactoryMap().putAll(Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap());
		resourceFactoryRegistry.getContentTypeToFactoryMap().putAll(Resource.Factory.Registry.INSTANCE.getContentTypeToFactoryMap());
		resourceFactoryRegistry.getProtocolToFactoryMap().putAll(Resource.Factory.Registry.INSTANCE.getProtocolToFactoryMap());
		rsfRegistration = ctx.getBundleContext().registerService(ResourceSetFactory.class, this, getDictionary());
	}
	
	/**
	 * Called on component deactivation
	 */
	@Deactivate
	public void deactivate() {
		if (rsfRegistration != null) {
			rsfRegistration.unregister();
			rsfRegistration = null;
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.ResourceSetFactory#createResourceSet()
	 */
	@Override
	public ResourceSet createResourceSet() {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.setPackageRegistry(packageRegistry);
		resourceSet.setResourceFactoryRegistry(resourceFactoryRegistry);
		resourceSetConfigurators.forEach((c)->c.configureResourceSet(resourceSet));
		return resourceSet;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.ResourceSetFactory#getResourceSetConfigurators()
	 */
	@Override
	public Collection<ResourceSetConfigurator> getResourceSetConfigurators() {
		return Collections.unmodifiableCollection(resourceSetConfigurators);
	}

	/**
	 * Updates the package registry
	 */
	private void updatePackageRegistry() {
		List<EPackageConfigurator> providers = new ArrayList<>(ePackageConfigurators);
		providers.forEach((p)->p.configureEPackage(packageRegistry));
	}
	
	/**
	 * Updates the resource factory registry
	 */
	private void updateResourceFactoryRegistry() {
		List<ResourceFactoryConfigurator> providers = new ArrayList<>(resourceFactoryConfigurators);
		providers.forEach((p)->p.configureResourceFactory(resourceFactoryRegistry));
	}
	
	/**
	 * Updates the properties of the service, depending on changes on injected services
	 * @param type the type of the property to publish 
	 * @param serviceProperties the service properties from the injected service
	 * @param add <code>true</code>, if the service was add, <code>false</code> in case of an remove
	 */
	private void updateProperties(String type, Map<String, Object> serviceProperties, boolean add) {
		Object name = serviceProperties.get(type);
		if (name != null && name instanceof String) {
			switch (type) {
			case EMFNamespaces.EMF_RESOURCE_CONFIGURATOR_NAME:
				if (add) {
					resourceFactoryNameList.add(name.toString());
				} else {
					resourceFactoryNameList.remove(name.toString());
				}
				break;
			case EMFNamespaces.EMF_CONFIGURATOR_NAME:
				if (add) {
					configuratorNameList.add(name.toString());
				} else {
					configuratorNameList.remove(name.toString());
				}
				break;
			case EMFNamespaces.EMF_MODEL_NAME:
				if (add) {
					modelNameList.add(name.toString());
				} else {
					modelNameList.remove(name.toString());
				}
				break;
			default:
				break;
			}
			updateRegistrationProperties();
		}
	}
	
	/**
	 * Updates the service registration properties
	 */
	private void updateRegistrationProperties() {
		if (rsfRegistration != null) {
			rsfRegistration.setProperties(getDictionary());
		}
	}
	
	/**
	 * Creates a dictionary for the stored properties
	 * @return a dictionary for the stored properties
	 */
	private Dictionary<String, Object> getDictionary() {
		Dictionary<String, Object> properties = new Hashtable<>();
		String[] configNames = configuratorNameList.toArray(new String[0]);
		String[] modelNames = modelNameList.toArray(new String[0]);
		String[] resourceFactoryNames = resourceFactoryNameList.toArray(new String[0]);
		properties.put(ComponentConstants.COMPONENT_NAME, "DefaultResourcesetFactory");
		properties.put(EMFNamespaces.EMF_CONFIGURATOR_NAME, configNames);
		properties.put(EMFNamespaces.EMF_MODEL_NAME, modelNames);
		properties.put(EMFNamespaces.EMF_RESOURCE_CONFIGURATOR_NAME, resourceFactoryNames);
		return properties;
	}

}
