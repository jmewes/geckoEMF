/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Hashtable;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.emf.osgi.ResourceSetCache;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Integration test for the {@link ResourceSetCache}
 * @author Mark Hoffmann
 * @since 25.07.2017
 */
@RunWith(MockitoJUnitRunner.class)
public class ResourceSetCacheIntegrationTest {
	
	private final BundleContext context = FrameworkUtil.getBundle(ResourceSetFactoryIntegrationTest.class).getBundleContext();

	@Before
	public void before() {
	}

	@After
	public void after() {
	}

	/**
	 * Tests, if the service was set up correctly
	 * @throws IOException 
	 * @throws InvalidSyntaxException 
	 * @throws InterruptedException 
	 */
	@Test
	public void testResourceSetCacheExists() throws IOException, InvalidSyntaxException, InterruptedException {
		Collection<ServiceReference<ResourceSetCache>> references = context.getServiceReferences(ResourceSetCache.class, null);
		assertNotNull(references);
		assertEquals(0, references.size());
		ServiceReference<ConfigurationAdmin> caRef = context.getServiceReference(ConfigurationAdmin.class);
		assertNotNull(caRef);
		ConfigurationAdmin ca = context.getService(caRef);
		assertNotNull(ca);
		Configuration configuration1 = ca.createFactoryConfiguration("ResourceSetCache", "?");
		assertNotNull(configuration1);
		Dictionary<String,Object> properties = configuration1.getProperties();
		assertNull(properties);
		Dictionary<String, Object> configProperties1 = new Hashtable<>();
		configProperties1.put(ResourceSetCache.RESOURCE_SET_CACHE_NAME, "test1");
		configuration1.update(configProperties1);
		String pid1 = configuration1.getPid();
		assertNotNull(pid1);
		
		ServiceReference<ResourceSetCache> cacheRef = getServiceReference(5000l, "(" + ResourceSetCache.RESOURCE_SET_CACHE_NAME + "=test1)");
		assertNotNull(cacheRef);
		
		assertEquals("test1", cacheRef.getProperty(ResourceSetCache.RESOURCE_SET_CACHE_NAME));
		ResourceSetCache cache1 = context.getService(cacheRef);
		assertNotNull(cache1);
		ResourceSet rs11 = cache1.getResourceSet();
		assertNotNull(rs11);
		ResourceSet rs21 = cache1.getResourceSet();
		assertNotNull(rs21);
		assertEquals(rs11,  rs21);
		
		Configuration configuration2 = ca.createFactoryConfiguration("ResourceSetCache", "?");
		assertNotNull(configuration2);
		Dictionary<String, Object> configProperties2 = new Hashtable<>();
		configProperties2.put(ResourceSetCache.RESOURCE_SET_CACHE_NAME, "test2");
		configuration2.update(configProperties2);
		String pid2 = configuration2.getPid();
		assertNotNull(pid2);
		assertNotSame(pid1, pid2);
		
		cacheRef = getServiceReference(5000l, "(" + ResourceSetCache.RESOURCE_SET_CACHE_NAME + "=test2)");
		assertNotNull(cacheRef);
		references = context.getServiceReferences(ResourceSetCache.class, null);
		assertEquals(2, references.size());
		assertEquals("test2", cacheRef.getProperty(ResourceSetCache.RESOURCE_SET_CACHE_NAME));
		ResourceSetCache cache2 = context.getService(cacheRef);
		
		assertNotEquals(cache1, cache2);
		
		assertNotNull(cache2);
		ResourceSet rs12 = cache2.getResourceSet();
		assertNotNull(rs12);
		ResourceSet rs22 = cache2.getResourceSet();
		assertNotNull(rs22);
		assertEquals(rs12,  rs22);
		
		assertNotEquals(rs11, rs12);
		
		configuration1.delete();
		configuration2.delete();
		
	}
	
	<T> ServiceReference<T> getServiceReference(long timeout, String filter) throws InterruptedException, InvalidSyntaxException {
		Filter f = FrameworkUtil.createFilter(filter);
		ServiceTracker<T, T> tracker = new ServiceTracker<>(context, f, null);
		tracker.open();
		tracker.waitForService(timeout);
		return tracker.getServiceReference();
	}

}
