/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.IOWrappedException;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.emf.osgi.EMFNamespaces;
import org.gecko.emf.osgi.EPackageConfigurator;
import org.gecko.emf.osgi.ResourceFactoryConfigurator;
import org.gecko.emf.osgi.ResourceSetConfigurator;
import org.gecko.emf.osgi.ResourceSetFactory;
import org.gecko.emf.osgi.model.test.Person;
import org.gecko.emf.osgi.model.test.TestFactory;
import org.gecko.emf.osgi.model.test.TestPackage;
import org.gecko.emf.osgi.model.test.configurator.TestPackageConfigurator;
import org.gecko.emf.osgi.tests.configurator.TestConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

/**
 * Tests the EMF OSGi integration
 * @author Mark Hoffmann
 * @since 25.07.2017
 */
@RunWith(MockitoJUnitRunner.class)
public class EMFOSGiIntegrationTest {

	private final BundleContext context = FrameworkUtil.getBundle(EMFOSGiIntegrationTest.class).getBundleContext();

	@Before
	public void before() {
	}

	@After
	public void after() {
	}

	/**
	 * Trying to load an instance with a non registered {@link EPackage}
	 * @throws IOException 
	 */
	@Test(expected=IOWrappedException.class)
	public void testLoadResourceFailNoEPackage() throws IOException {
		ServiceReference<ResourceSetFactory> reference = context.getServiceReference(ResourceSetFactory.class);
		assertNotNull(reference);
		Object modelNames = reference.getProperty(EMFNamespaces.EMF_MODEL_NAME);
		assertNotNull(modelNames);
		assertTrue(modelNames instanceof String[]);
		List<String> modelNameList = Arrays.asList((String[]) modelNames);
		assertTrue(modelNameList.contains("ecore"));
		assertFalse(modelNameList.contains("test"));
		
		ResourceSetFactory factory = context.getService(reference);
		assertNotNull(factory);
		ResourceSet rs = factory.createResourceSet();
		assertNotNull(rs);
		URI uri = URI.createURI("person.test");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Resource testSaveResource = rs.createResource(uri);
		assertNotNull(testSaveResource);
		Person p = TestFactory.eINSTANCE.createPerson();
		p.setFirstName("Emil");
		p.setLastName("Tester");
		testSaveResource.getContents().add(p);
		testSaveResource.save(baos, null);
		
		byte[] content = baos.toByteArray();
		ByteArrayInputStream bais = new ByteArrayInputStream(content);
		
		Resource testLoadResource = rs.createResource(uri);
		testLoadResource.load(bais, null);
	}
	
	/**
	 * Trying to load an instance with a registered {@link EPackage}
	 * @throws IOException 
	 */
	@Test
	public void testLoadResourceRegisteredEPackage() throws IOException {
		
		context.registerService(new String[] {EPackageConfigurator.class.getName(), ResourceFactoryConfigurator.class.getName()}, new TestPackageConfigurator(), null);
		
		ServiceReference<ResourceSetFactory> reference = context.getServiceReference(ResourceSetFactory.class);
		assertNotNull(reference);
		ResourceSetFactory factory = context.getService(reference);
		assertNotNull(factory);
		ResourceSet rs = factory.createResourceSet();
		assertNotNull(rs);
		URI uri = URI.createURI("person.test");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Resource testSaveResource = rs.createResource(uri);
		assertNotNull(testSaveResource);
		Person p = TestFactory.eINSTANCE.createPerson();
		p.setFirstName("Emil");
		p.setLastName("Tester");
		testSaveResource.getContents().add(p);
		testSaveResource.save(baos, null);
		
		byte[] content = baos.toByteArray();
		ByteArrayInputStream bais = new ByteArrayInputStream(content);
		
		Resource testLoadResource = rs.createResource(uri);
		assertEquals(0, testLoadResource.getContents().size());
		testLoadResource.load(bais, null);
		assertEquals(1, testLoadResource.getContents().size());
		Person result = (Person) testLoadResource.getContents().get(0);
		assertNotNull(result);
		assertEquals("Emil", result.getFirstName());
		assertEquals("Tester", result.getLastName());
	}
	
	/**
	 * Trying to load an instance with a registered {@link EPackage} than unload the EPackage and have an exception
	 * @throws IOException 
	 */
	@Test(expected=IOWrappedException.class)
	public void testLoadResourceRegisteredEPackageAndUnregister() throws IOException {
		
		ServiceRegistration<?> registration = context.registerService(new String[] {EPackageConfigurator.class.getName(), ResourceFactoryConfigurator.class.getName()}, new TestPackageConfigurator(), null);
		
		ServiceReference<ResourceSetFactory> reference = context.getServiceReference(ResourceSetFactory.class);
		assertNotNull(reference);
		
		ResourceSetFactory factory = context.getService(reference);
		assertNotNull(factory);
		ResourceSet rs = factory.createResourceSet();
		assertNotNull(rs);
		URI uri = URI.createURI("person.test");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Resource testSaveResource = rs.createResource(uri);
		assertNotNull(testSaveResource);
		Person p = TestFactory.eINSTANCE.createPerson();
		p.setFirstName("Emil");
		p.setLastName("Tester");
		testSaveResource.getContents().add(p);
		testSaveResource.save(baos, null);
		
		byte[] content = baos.toByteArray();
		ByteArrayInputStream bais = new ByteArrayInputStream(content);
		
		Resource testLoadResource = rs.createResource(uri);
		assertEquals(0, testLoadResource.getContents().size());
		testLoadResource.load(bais, null);
		assertEquals(1, testLoadResource.getContents().size());
		Person result = (Person) testLoadResource.getContents().get(0);
		assertNotNull(result);
		assertEquals("Emil", result.getFirstName());
		assertEquals("Tester", result.getLastName());
		
		registration.unregister();
		
		Resource testLoadResource2 = rs.createResource(uri);
		assertNotEquals(testLoadResource, testLoadResource2);
		assertEquals(0, testLoadResource2.getContents().size());
		testLoadResource2.load(bais, null);
	}
	
	/**
	 * Trying to load an instance with a registered {@link EPackage} than unload the EPackage and have an exception
	 * @throws IOException 
	 */
	@Test
	public void testLoadResourceRegisteredEPackageAndUnregisterProperties() throws IOException {
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put(EMFNamespaces.EMF_MODEL_NAME, TestPackage.eNAME);
		ServiceRegistration<?> registration = context.registerService(new String[] {EPackageConfigurator.class.getName(), ResourceFactoryConfigurator.class.getName()}, new TestPackageConfigurator(), properties);
		
		ServiceReference<ResourceSetFactory> reference = context.getServiceReference(ResourceSetFactory.class);
		assertNotNull(reference);
		Object modelNames = reference.getProperty(EMFNamespaces.EMF_MODEL_NAME);
		assertNotNull(modelNames);
		assertTrue(modelNames instanceof String[]);
		List<String> modelNameList = Arrays.asList((String[]) modelNames);
		assertTrue(modelNameList.contains("ecore"));
		assertTrue(modelNameList.contains("test"));
		
		ResourceSetFactory factory = context.getService(reference);
		assertNotNull(factory);
		ResourceSet rs = factory.createResourceSet();
		assertNotNull(rs);
		URI uri = URI.createURI("person.test");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Resource testSaveResource = rs.createResource(uri);
		assertNotNull(testSaveResource);
		Person p = TestFactory.eINSTANCE.createPerson();
		p.setFirstName("Emil");
		p.setLastName("Tester");
		testSaveResource.getContents().add(p);
		testSaveResource.save(baos, null);
		
		byte[] content = baos.toByteArray();
		ByteArrayInputStream bais = new ByteArrayInputStream(content);
		
		Resource testLoadResource = rs.createResource(uri);
		assertEquals(0, testLoadResource.getContents().size());
		testLoadResource.load(bais, null);
		assertEquals(1, testLoadResource.getContents().size());
		Person result = (Person) testLoadResource.getContents().get(0);
		assertNotNull(result);
		assertEquals("Emil", result.getFirstName());
		assertEquals("Tester", result.getLastName());
		
		registration.unregister();
		
		Resource testLoadResource2 = rs.createResource(uri);
		assertNotEquals(testLoadResource, testLoadResource2);
		assertEquals(0, testLoadResource2.getContents().size());
		try {
			testLoadResource2.load(bais, null);
			fail("IOWrappedException exptected");
		} catch (IOWrappedException e) {
			assertNotNull(e);
		}
		modelNames = reference.getProperty(EMFNamespaces.EMF_MODEL_NAME);
		assertNotNull(modelNames);
		assertTrue(modelNames instanceof String[]);
		modelNameList = Arrays.asList((String[]) modelNames);
		assertTrue(modelNameList.contains("ecore"));
		assertFalse(modelNameList.contains("test"));
	}
	
	/**
	 * Trying to load an instance with a registered {@link EPackage} than unload the EPackage and have an exception
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	@Test
	public void testEPackageAndConfiguratorProperties() throws IOException, InterruptedException {
		Dictionary<String, Object> epackageProperties = new Hashtable<String, Object>();
		epackageProperties.put(EMFNamespaces.EMF_MODEL_NAME, TestPackage.eNAME);
		ServiceRegistration<?> epackageRegistration = context.registerService(new String[] {EPackageConfigurator.class.getName(), ResourceFactoryConfigurator.class.getName()}, new TestPackageConfigurator(), epackageProperties);
		
		ServiceReference<ResourceSetFactory> reference = context.getServiceReference(ResourceSetFactory.class);
		assertNotNull(reference);
		Object modelNames = reference.getProperty(EMFNamespaces.EMF_MODEL_NAME);
		assertNotNull(modelNames);
		assertTrue(modelNames instanceof String[]);
		List<String> modelNameList = Arrays.asList((String[]) modelNames);
		assertTrue(modelNameList.contains("ecore"));
		assertTrue(modelNameList.contains("test"));
		Object configNames = reference.getProperty(EMFNamespaces.EMF_CONFIGURATOR_NAME);
		assertNotNull(configNames);
		assertTrue(configNames instanceof String[]);
		List<String> configNameList = Arrays.asList((String[]) configNames);
		assertEquals(0, configNameList.size());

		Dictionary<String, Object> configProperties = new Hashtable<String, Object>();
		configProperties.put(EMFNamespaces.EMF_CONFIGURATOR_NAME, "testConfigurator");
		configProperties.put(EMFNamespaces.EMF_RESOURCE_CONFIGURATOR_NAME, "testResourceConfigurator");
		ServiceRegistration<?> configRegistration = context.registerService(new String[] {ResourceSetConfigurator.class.getName(), ResourceFactoryConfigurator.class.getName()}, new TestConfigurator(), configProperties);
		Thread.sleep(1000l);
		
		configNames = reference.getProperty(EMFNamespaces.EMF_CONFIGURATOR_NAME);
		assertNotNull(configNames);
		assertTrue(configNames instanceof String[]);
		configNameList = Arrays.asList((String[]) configNames);
		assertEquals(1, configNameList.size());
		assertTrue(configNameList.contains("testConfigurator"));
		
		configNames = reference.getProperty(EMFNamespaces.EMF_RESOURCE_CONFIGURATOR_NAME);
		assertNotNull(configNames);
		assertTrue(configNames instanceof String[]);
		configNameList = Arrays.asList((String[]) configNames);
		assertEquals(1, configNameList.size());
		assertTrue(configNameList.contains("testResourceConfigurator"));
		
		ResourceSetFactory factory = context.getService(reference);
		assertNotNull(factory);
		ResourceSet rs = factory.createResourceSet();
		assertNotNull(rs);
		URI uri = URI.createURI("person.test");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Resource testSaveResource = rs.createResource(uri);
		assertNotNull(testSaveResource);
		Person p = TestFactory.eINSTANCE.createPerson();
		p.setFirstName("Emil");
		p.setLastName("Tester");
		testSaveResource.getContents().add(p);
		testSaveResource.save(baos, null);
		
		byte[] content = baos.toByteArray();
		ByteArrayInputStream bais = new ByteArrayInputStream(content);
		
		Resource testLoadResource = rs.createResource(uri);
		assertEquals(0, testLoadResource.getContents().size());
		testLoadResource.load(bais, null);
		assertEquals(1, testLoadResource.getContents().size());
		Person result = (Person) testLoadResource.getContents().get(0);
		assertNotNull(result);
		assertEquals("Emil", result.getFirstName());
		assertEquals("Tester", result.getLastName());
		
		epackageRegistration.unregister();
		
		modelNames = reference.getProperty(EMFNamespaces.EMF_MODEL_NAME);
		assertNotNull(modelNames);
		assertTrue(modelNames instanceof String[]);
		modelNameList = Arrays.asList((String[]) modelNames);
		assertTrue(modelNameList.contains("ecore"));
		assertFalse(modelNameList.contains("test"));
		
		configRegistration.unregister();
		Thread.sleep(500l);
		
		configNames = reference.getProperty(EMFNamespaces.EMF_CONFIGURATOR_NAME);
		assertNotNull(configNames);
		assertTrue(configNames instanceof String[]);
		configNameList = Arrays.asList((String[]) configNames);
		assertEquals(0, configNameList.size());
		assertFalse(configNameList.contains("testConfigurator"));

		configNames = reference.getProperty(EMFNamespaces.EMF_RESOURCE_CONFIGURATOR_NAME);
		assertNotNull(configNames);
		assertTrue(configNames instanceof String[]);
		configNameList = Arrays.asList((String[]) configNames);
		assertEquals(0, configNameList.size());
		assertFalse(configNameList.contains("testResourceConfigurator"));
	}

}