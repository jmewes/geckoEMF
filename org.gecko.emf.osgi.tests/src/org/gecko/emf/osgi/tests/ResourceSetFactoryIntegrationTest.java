/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.tests;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.emf.osgi.ResourceSetFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

/**
 * Integration test for the {@link ResourceSetFactory}
 * @author Mark Hoffmann
 * @since 25.07.2017
 */
@RunWith(MockitoJUnitRunner.class)
public class ResourceSetFactoryIntegrationTest {

	private final BundleContext context = FrameworkUtil.getBundle(ResourceSetFactoryIntegrationTest.class).getBundleContext();

	@Before
	public void before() {
	}

	@After
	public void after() {
	}

	/**
	 * Tests, if the service was set up correctly
	 */
	@Test
	public void testResourceSetFactoryExists() {
		ServiceReference<ResourceSetFactory> reference = context.getServiceReference(ResourceSetFactory.class);
		assertNotNull(reference);
		ResourceSetFactory factory = context.getService(reference);
		assertNotNull(factory);
		ResourceSet rs1 = factory.createResourceSet();
		assertNotNull(rs1);
		ResourceSet rs2 = factory.createResourceSet();
		assertNotNull(rs2);
		assertNotEquals(rs1,  rs2);
	}
	
}